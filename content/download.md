---
title: Download
sassFiles:
  - /scss/falkon_download.scss
menu:
  main:
    weight: 2
---

{{< get_it type="linux" >}}
### Linux

+ Install [Falkon](https://apps.kde.org/falkon) from [your distribution](https://kde.org/distributions/).
    {{< store_badge type="appstream" link="appstream://org.kde.falkon.desktop" divClass="store-badge" imgClass="store-badge-img" >}}

- Install [Falkon's Snap package from Snapcraft](https://snapcraft.io/falkon).
    {{< store_badge type="snapstore" link="https://snapcraft.io/falkon" divClass="store-badge" imgClass="store-badge-img" >}}

+ Install [Falkon's Flatpak package from Flathub](https://flathub.org/apps/details/org.kde.falkon).
    {{< store_badge type="flathub" link="https://flathub.org/apps/details/org.kde.falkon" divClass="store-badge" imgClass="store-badge-img" >}}

{{< /get_it >}}


{{< get_it type="source" >}}
### Source Code

The [source code for Falkon](https://invent.kde.org/network/falkon) is available on KDE’s GitLab instance.  
For instructions on how to build Falkon from source, check the [Development](/development) page.

{{< /get_it >}}


### About the releases
[Falkon](https://apps.kde.org/falkon) is a part of [KDE Applications](https://apps.kde.org) and **KDE Gear**, which are [released typically 3 times a year en-masse](https://community.kde.org/Schedules). New releases are announced [here](https://kde.org/announcements/).
