---
title: Frequently Asked Questions
menu:
  main:
    weight: 10
    name: FAQ
faq:
  - question: "Unable to pass CloudFlare captcha"
    answer: "Use the default user agent for the site and enable 'Allow reading from canvas' either globally or for the site and reload the page."
  - question: "Maps sites such as DuckDuckGo, Bing and Google Maps are not displayed properly."
    answer: "Enable 'Allow reading from canvas' either globally or for the site and reload the page."
---

{{< faq name="faq" >}}
---
