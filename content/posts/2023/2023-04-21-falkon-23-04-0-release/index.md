---
layout: post
title:  "Falkon 23.04.0 released"
date:   2023-04-21 12:00:00 +0000
comments: true
---

New Falkon version 23.04.0 is being released as part of KDE Gear.

### Notable changes

There is a handful of changes in this release.

#### KWallet

The format under which the passwords are stored has changed from Binary to Map.
The passwords can now be viewed from within KWalletManager and even edited.
While editing and adding new ones I would be careful with the `data` field and updated that as well. (This is some Falkon password internal mechanic)
The Folder under which the passwords are stored changed from `Falkon` to `FalkonPasswords`.
This was done to not overwrite the old passwords and potentialy ruin them during the migration to new format.

#### Support for dark color scheme

> **Note:** This is NOT browser dark mode support.

Falkon internal pages now respect the dark color scheme, if it is forced through chromium flags in environment variable.

A small demonstration can be seen below. The color scheme might not be the best since it was designed on display with weird color settings.

<figure class="image">
  <img src="/posts/2023/2023-04-21-falkon-23-04-0-release/images/falkon_light_and_dark_page.png" alt="Falkon: Light and Dark styled page">
  <figcaption>Falkon: Light and Dark styled page</figcaption>
</figure>


### Changelog

* Look for spellchecking dictionaries at location specified by QTWEBENGINE_DICTIONARIES_PATH environment variable
* Show QtWebEngine version on Falkon Config page
* Implement prefers-color-scheme (dark mode) for internal pages
* Add option to enable GPU acceleration (by Hao Chi Kiang)
* PyFalkon: addBookmark - make C++ own parameters (fixes potential crash)
* KWallet: Store passwords in a map format
* History: Don't delete all items under dates when filtering
* AdBlock: Workaround for "Blocked content" page
* AddressBar: Search with default search engine by default
* Fix crash when adding new folder to the bookmark toolbar
* Fix: Bookmarks folder disappears when moving it onto itself



**Download**: [ffalkon-23.04.0.tar.xz](https://download.kde.org/stable/release-service/23.04.0/src/falkon-23.04.0.tar.xz) ([sig](https://download.kde.org/stable/release-service/23.04.0/src/falkon-23.04.0.tar.xz.sig) signed with [EBC3FC294452C6D8](/signing-keys/esk-riddell.gpg))
