---
layout: post
title:  "Falkon 24.12 Release notes"
date:   2024-12-15 12:00:00 +0000
comments: true
---

This release includes fixes for GreaseMonkey, VerticalTabs, Navigation
bar (security icon), stability fixes, does not advertise the FTP
support, fixes printing and more small fixes.

<!--more-->

## GreaseMonkey

1. Fixed loading of sites ending with `*.user.js` name.
   {{% bug id="467459" %}}
2. GreaseMonkey got a support for running scripts on sites through
   context menu. {{% bug id="469855" %}}

{{< img src="/posts/2024/2024-12-15-falkon-24-12-release-notes/images/greasemonkey-context-menu.png" caption="GreaseMonkey context menu" >}}


## VerticalTabs

1. Enabled switching tabs with mouse wheel when the scrollbar is
   visible. {{% bug id="394066" %}}
2. Pinned tabs are now arranged vertically like a normal tabs in this
   plugin. {{% bug id="452818" %}}

{{< img src="/posts/2024/2024-12-15-falkon-24-12-release-notes/images/verticaltabs-pinnedtabs.png" caption="VerticalTabs: Pinned tabs" >}}


## Site security icon

Falkon now display more proper security icon in the url bar based on
the state if the SSL certificate exceptions. {{% bug id="420902" %}}

{{< img src="/posts/2024/2024-12-15-falkon-24-12-release-notes/images/security-icon.png" caption="Various security icons" >}}


## Removed FTP support

The FTP support in Chomium and QtWebEngine which is based on it was
removed a while ago. So with this update Falkon will not advertise to
the system that it support FTP protocol and it will instead try to
open other program to handle it. {{% bug id="494222" %}}


## Others

And more:
- Fix printing to real CUPS printer. {{% bug id="497051" %}}
- Preferences: Fix crash when un/loading of plugins
  {{% bug id="492023" %}}
- Random fixes


## Short changelog

* Do not advertise ftp support to the system {{% bug id="494222" %}}
* GreaseMonkey: Add support for context menu {{% bug id="469855" %}}
* GreaseMonkey: Check content type of url *.user.js
  {{% bug id="467459" %}}
* VerticalTabs: Fix tab switching with mouse wheel
  {{% bug id="394066" %}}
* VerticalTabs: Arrange pinned tabs vertically {{% bug id="452818" %}}
* Set security icon according to certificate error.
  {{% bug id="420902" %}}
* Preferences: Fix crash when un/loading of plugins
  {{% bug id="492023" %}}
* Fix printing to real CUPS printer {{% bug id="497051" %}}
* Random fixes
