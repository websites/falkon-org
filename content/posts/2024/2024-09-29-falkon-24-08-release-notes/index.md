---
layout: post
title:  "Falkon 24.08 Release notes"
date:   2024-09-29 12:00:00 +0000
comments: true
---

This release focuses on SiteSettings feature, so I will try to
introduce it here.

<!--more-->

## Site Settings

This feature tries to enable per site configuration / settings of
multiple options per website host / domain. It consist of having
global / default options and site / local settings.

### List of supported configuration options

- Cookies
- HTML5 permissions
  - Notifications
  - Geolocation
  - Microphone
  - Camera
  - Microphone and Camera
  - Hide mouse pointer
  - Screen capture
  - Screen capture with audio
- QtWebEngine permissions
  - Autoload Images
  - Enable Javascript
  - Javascript: Open popup windows
  - Javascript: Access clipboard
  - Javascript: Paste from clipboard
  - JavaScript: Activate windows
  - Local Storage
  - FullScreen support
  - Run insecure content
  - Automatic playing of videos
  - Allow reading from canvas (Needs to be enabled to pass CloudFlare) (Qt 6.6+)
  - Force dark mode (Qt 6.7+)

### Previews

These options can be changed for the given site in the "Site Info"
dialog.
<figure class="image">
  <img src="/posts/2024/2024-09-29-falkon-24-08-release-notes/images/siteinfo-permissions.png" alt="Site Info with Permissions tab">
  <figcaption>Site Info with Permissions tab</figcaption>
</figure>

The default options for HTML5 can be changed in "Preferences > Privacy"
<figure class="image">
  <img src="/posts/2024/2024-09-29-falkon-24-08-release-notes/images/preferences-privacy-html-permissions.png" alt="Preferences with HTML5 permissions tab">
  <figcaption>Preferences with Privacy and HTML5 Permissions tab</figcaption>
</figure>

The default QtWebEngine / site options can be changed in the Browsing
tab or in place where they where configured until now.

<figure class="image">
  <img src="/posts/2024/2024-09-29-falkon-24-08-release-notes/images/preferences-privacy-site-settings.png" alt="Preferences with Site Settings tab">
  <figcaption>Preferences with Privacy and Site Settings tab</figcaption>
</figure>

In the preferences it is only possible to change existing modified
permissions for given sites. At the moment it is not possible to
manually add sites to the list, this can only be done from within
"Site Info" dialog.


### Missing feature

These features will be done at a later unspecified date.

- QML support
- Zoom level per domain


## Short ChangeLog

* Add site permissions
* Enable WebInspector with disabled JavaScript (BUG: 462887)
* Random fixes

