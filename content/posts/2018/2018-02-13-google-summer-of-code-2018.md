---
layout: post
title:  "Google Summer of Code 2018"
date:   2018-02-13 02:50:12 +0100
comments: true
---
KDE has been [accepted](https://twitter.com/kdecommunity/status/963152003193958406) as a mentoring organization for Google Summer of Code again this year. If you are a student, check out [ideas list](https://community.kde.org/GSoC/2018/Ideas) and submit your proposals!

There are currently also two ideas for Falkon, which you may apply for:

* JavaScript/QML extensions support
* Plasma integration

