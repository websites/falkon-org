---
layout: post
title:  "Falkon 3.1.0 released"
date:   2019-03-19 18:01:52 +0100
comments: true
---
New Falkon version is now out.

#### Changelog

* add support for writing plugins in QML
* add MiddleClickLoader Python plugin
* add page sharing to KDE Frameworks Integration plugin
* add basic support for client certificates (QtWebEngine >= 5.12)
* add support for registering custom protocol handlers (QtWebEngine >= 5.12)
* change search bar appearance to match other KDE apps
* use DBus instead of lockfile/socket to check and communicate with other instances (Linux) [#404494](https://bugs.kde.org/404494)
* whitelisted cookies are no longer deleted when deleting all cookies
* Python plugin support is now stable
* close entire tree after middle click on collapsed tabs in VerticalTabs plugin [#399217](https://bugs.kde.org/399217)
* fix some compatibility issues with QtWebEngine up to 5.12
* fix some possible crashes in AdBlock
* fix builds with standalone QtWebEngine releases

**Download**: [falkon-3.1.0.tar.xz](http://download.kde.org/stable/falkon/3.1/falkon-3.1.0.tar.xz) ([sig](http://download.kde.org/stable/falkon/3.1/falkon-3.1.0.tar.xz.sig) signed with [EBC3FC294452C6D8](https://sks-keyservers.net/pks/lookup?op=vindex&search=0xEBC3FC294452C6D8))
