---
title: Falkon
---

        <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">

              <div style="vertical-align:center;display:inline-block;text-align:center;max-width:100%">
                  <img style="max-width:100%" src="{{ "/images/screenshot.png" | prepend: site.baseurl }}">
                  <h3 style="margin:0">Current version: 3.2.0</h3>
                  <h4>(released 31 January 2022)</h4>
                  <a style="font-size:16px;" href="{{ "/download" | prepend: site.baseurl }}" class="btn btn-primary btn-sm">Download</a>
              </div>

              <div style="margin-top:70px">
                {% for post in site.posts %}
                  {% if forloop.index > 3 %}{% break %}{% endif %}
                  <h3><a href="{{ post.url | prepend: site.baseurl }}">{{ post.title }}</a></h3>
                  <p><i class="fa fa-calendar"></i> &nbsp;{{ post.date | date: "%b %-d, %Y" }} {% if site.disqus_short_name and page.comments != false and site.disqus_show_comment_count == true %}
        |  <a href="{% if page.index %}{{ root_url }}{{ post.url }}{% endif %}#disqus_thread">Comments</a>
        {% endif %}</p>
                {% endfor %}

                <h4 style="padding-top:10px"><a href="{{ "/archive" | prepend: site.baseurl }}">More...</a></h4>
              </div>
            </div>
            <div class="col-md-3"></div>
         </div>

